#!/bin/bash

set -xueo pipefail

cd /build

# Enable Emacs PPA
tee /etc/apt/sources.list.d/deadsnakes.list <<EOF
deb http://ppa.launchpad.net/kelleyk/emacs/ubuntu focal main
EOF
apt-key adv --keyserver keyserver.ubuntu.com \
        --recv-keys 873503A090750CDAEB0754D93FF0E01EEAAFC9CD

# Update apt database and upgrade if needed
apt-get update
apt-get -y upgrade

# Install Python packages
DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install emacs27

rm -rf /var/lib/apt/lists/*

# Put .desktop file template in place.
install -m 0644 emacs.desktop.in /etc/etui/applications/emacs.desktop.in
