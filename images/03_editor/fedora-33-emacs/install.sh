#!/bin/bash

set -xueo pipefail

cd /build

# Update existing packages
dnf -y update

# Install Emacs
dnf -y --setopt=install_weak_deps=False install emacs

# Clean up DNF cache
dnf clean all
rm -rf /var/cache/dnf

# Put .desktop file template in place.
install -m 0755 -d /etc/etui/applications
install -m 0644 emacs.desktop.in /etc/etui/applications/emacs.desktop.in
