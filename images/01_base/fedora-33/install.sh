#!/bin/bash

set -xueo pipefail

cd /build

# Enable installation of documentation in the container
sed -i '/tsflags=nodocs/d' /etc/dnf/dnf.conf

# Update existing packages
dnf -y update

# Reinstall some packages to get missing documentation
dnf -y --setopt=install_weak_deps=False reinstall \
    bash \
    curl \
    gawk \
    grep \
    gzip \
    libcap \
    p11-kit \
    pam \
    python3 \
    rpm \
    sed \
    tar

# Install generally useful packages.
dnf -y --setopt=install_weak_deps=False install \
    @development-tools \
    @c-development \
    bash-completion \
    bc \
    bind-utils \
    breezy \
    bzip2 \
    diffutils \
    dos2unix \
    file \
    findutils \
    flatpak-spawn \
    fpaste \
    gettext \
    git \
    glibc-all-langpacks \
    hostname \
    iputils \
    jq \
    jwhois \
    less \
    libcurl-devel \
    lsof \
    man-db \
    man-pages \
    moreutils \
    mtr \
    openssh-clients \
    openssl-devel \
    passwd \
    procps-ng \
    python-unversioned-command \
    rsync \
    sha \
    shadow-utils \
    ShellCheck \
    sshpass \
    subversion \
    sudo \
    tcpdump \
    time \
    traceroute \
    unzip \
    vte-profile \
    which \
    xmlstarlet \
    xorg-x11-xauth \
    xz \
    zip

# Clean up DNF cache
dnf clean all
rm -rf /var/cache/dnf

# Allow passwordless sudo
tee /etc/sudoers.d/etui <<EOF
%wheel	ALL=(ALL)	NOPASSWD: ALL
EOF

# Put setup script in place
install -m 0755 -d /etc/etui
install -m 0755 setup.sh /etc/etui/setup
install -m 0755 -d /etc/etui/setup.d

# Put .desktop file template in place.
install -m 0755 -d /etc/etui/applications
install -m 0644 terminal.desktop.in /etc/etui/applications/terminal.desktop.in
