#!/bin/bash

set -ueo pipefail

# Script triggered by Etui to do necessary setup within the container.

while getopts "p:u:i:g:w:h:" OPTION; do
    case "$OPTION" in
        p)
            project="$OPTARG"
            ;;
        u)
            username="$OPTARG"
            ;;
        i)
            uid="$OPTARG"
            ;;
        g)
            gid=$OPTARG
            ;;
        w)
            workspace="$OPTARG"
            ;;
        h)
            home="$OPTARG"
            ;;
        *)
            exit
            ;;
    esac
done

# Update user to have correct properties.
usermod --home "$home" --groups sudo --shell /bin/bash "$username"

# Fix ownership of home directory.
chown "$uid:$gid" "$home"

# Fix permissions of parent directories to workspace.
cd "$workspace"/..
while [[ $(realpath "$PWD") != $(realpath "$home") ]] && [[ $PWD != / ]]; do
    chown "$uid:$gid" .
    cd ..
done

# Create file to indicate that this is an Etui container.
touch /var/run/.etuienv

# Set environment variable in profile to provide the project name within the
# container.
tee /etc/profile.d/etui.sh <<EOF
export ETUI_PROJECT="$project"
export ETUI_WORKSPACE="$workspace"
EOF

# Expand variables in .desktop file templates.
for f in /etc/etui/applications/*.desktop.in; do
    env -i PROJECT="$project" WORKSPACE="$workspace" envsubst < "$f" > "${f%.in}"
    rm "$f"
done

# Source any additional setup files added by child images.
find /etc/etui/setup.d -maxdepth 1 -name '*.sh' | while read -r f; do
    # shellcheck source=/dev/null
    source "$f"
done
