#!/bin/bash

set -xueo pipefail

cd /build

apt-get update
apt-get -y upgrade
echo y | unminimize

# Dependency of apt-key
DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install \
        autoconf \
        automake \
        bash-completion \
        build-essential \
        bzip2 \
        bzr \
        ca-certificates \
        curl \
        default-libmysqlclient-dev \
        dirmngr \
        dpkg-dev \
        file \
        flatpak \
        g++ \
        gcc \
        gettext \
        git \
        gnupg \
        imagemagick \
        less \
        libbz2-dev \
        libc6-dev \
        libcurl4-openssl-dev \
        libdb-dev \
        libevent-dev \
        libffi-dev \
        libgdbm-dev \
        libglib2.0-dev \
        libgmp-dev \
        libjpeg-dev \
        libkrb5-dev \
        liblzma-dev \
        libmagickcore-dev \
        libmagickwand-dev \
        libmaxminddb-dev \
        libncurses5-dev \
        libncursesw5-dev \
        libpng-dev \
        libpq-dev \
        libreadline-dev \
        libsqlite3-dev \
        libssl-dev \
        libtool \
        libwebp-dev \
        libxml2-dev \
        libxslt-dev \
        libyaml-dev \
        locales-all \
        make \
        man-db \
        manpages \
        mercurial \
        moreutils \
        netbase \
        openssh-client \
        patch \
        procps \
        subversion \
        sudo \
        unzip \
        wget \
        xz-utils \
        zlib1g-dev

rm -rf /var/lib/apt/lists/*

# Allow passwordless sudo
tee /etc/sudoers.d/etui <<"EOF"
%sudo ALL=(ALL) NOPASSWD: ALL
EOF
chmod 0440 /etc/sudoers.d/etui

# Put setup script in place
install -m 0755 -d /etc/etui
install -m 0755 setup.sh /etc/etui/setup
install -m 0755 -d /etc/etui/setup.d

# Put .desktop file template in place.
install -m 0755 -d /etc/etui/applications
install -m 0644 terminal.desktop.in /etc/etui/applications/terminal.desktop.in
