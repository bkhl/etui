#!/bin/bash

set -xueo pipefail

cd /build

# Enable Deadsnakes PPA
tee /etc/apt/sources.list.d/deadsnakes.list <<EOF
deb http://ppa.launchpad.net/deadsnakes/ppa/ubuntu focal main
EOF
apt-key adv --keyserver keyserver.ubuntu.com \
        --recv-keys F23C5A6CF475977595C89F51BA6932366A755776

# Update apt database and upgrade if needed
apt-get update
apt-get -y upgrade

# Install Python packages
DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install \
        python2.? \
        python2.?-dev \
        python3-dev \
        python3-distutils \
        python3-pip\
        python3-setuptools \
        python3.? \
        python3.?-dev \
        python3.?-venv

rm -rf /var/lib/apt/lists/*

# Install Poetry
curl --proto '=https' --tlsv1.2 --silent --show-error --fail --remote-name \
     --location 'https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py'
HOME=/build POETRY_HOME=/opt/poetry python3 get-poetry.py
chmod 0755 /opt/poetry/bin/poetry

tee /etc/profile.d/poetry.sh <<"EOF"
source /opt/poetry/env
EOF
