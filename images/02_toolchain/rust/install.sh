#!/bin/bash

set -xueo pipefail

cd /build

export RUSTUP_HOME=/opt/rustup
export CARGO_HOME=/opt/cargo

# Install Rustup
curl --proto '=https' --tlsv1.2 --silent --show-error --fail \
     --location 'https://sh.rustup.rs' \
     --output rustup-init.sh
HOME=/build sh rustup-init.sh -y

# Install rust-analyzer
curl --proto '=https' --tlsv1.2 --silent --show-error --fail \
     --location 'https://github.com/rust-analyzer/rust-analyzer/releases/latest/download/rust-analyzer-linux' \
     --output /usr/local/bin/rust-analyzer
chmod 0755 /usr/local/bin/rust-analyzer

# Make setup script change ownership of Rustup/Cargo so that the user can
# install additional packages.
tee /etc/etui/setup.d/rust.sh <<EOF
chown -R "\${uid}:\${gid}" "${RUSTUP_HOME}" "${CARGO_HOME}"
EOF

# Add Cargo initialization to shell profile.
tee /etc/profile.d/rust.sh <<EOF
export RUSTUP_HOME="$RUSTUP_HOME"
export CARGO_HOME="$CARGO_HOME"
export PATH="\$CARGO_HOME/bin\${PATH:+:\$PATH}"
EOF

# Print versions
/opt/cargo/bin/rustup --version
/opt/cargo/bin/cargo --version
/opt/cargo/bin/rustc --version
/usr/local/bin/rust-analyzer --version
